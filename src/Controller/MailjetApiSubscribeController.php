<?php

namespace Drupal\mailjet_api_subscribe\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for email confirmer module routes.
 */
class MailjetApiSubscribeController extends ControllerBase {

  /**
   * Resend confirmation email.
   *
   */
  public function test() {
    return [
      '#markup' => 'hello',
    ];
  }

}
