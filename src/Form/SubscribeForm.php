<?php

namespace Drupal\mailjet_api_subscribe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailjet_api_subscribe\Confirmer\ConfirmerInterface;
use Drupal\mailjet_api_subscribe\Confirmer\ConfirmerPluginManager;
use Drupal\mailjet_api_subscribe\MailjetApiSubscribeHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 *  The actual frontend subscribe widget, which is always embedded via a block
 */
class SubscribeForm extends FormBase {

  protected $submitLabel;

  protected $listID;

  protected $confirmerPlugin;

  protected $mailjetApiSubscribe;

  protected $pluginManager;


  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailjet_api_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email'] = [
      '#type'       => 'email',
      '#size'       => 25,
      '#attributes' => [
        'placeholder' => $this->t('Email Address'),
      ],
    ];
    $form['list_id'] = [
      '#type'  => 'hidden',
      '#value' => $this->listID,
    ];
    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->submitLabel,
    ];
    $form = $this->confirmerPlugin->buildForm($form, $form_state);
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->confirmerPlugin->validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->confirmerPlugin->submitForm($form, $form_state);
  }


  public function setSubmitLabel(string $submit_label) {
    $this->submitLabel = $submit_label;
  }

  public function setListID(string $list_id) {
    $this->listID = $list_id;
  }

  public function setConfirmer($confirmer) {
    if (is_string($confirmer)) {
      $this->confirmerPlugin = $this->pluginManager->createInstance($confirmer);
    }
    else {
      if ($confirmer instanceof ConfirmerInterface) {
        $this->confirmerPlugin = $confirmer;
      }
    }

    if (!empty($this->confirmerPlugin)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function setMailjetApiSubscribe(MailjetApiSubscribeHandler $mailjet_api_subscribe) {
    $this->mailjetApiSubscribe = $mailjet_api_subscribe;
  }

  public function setConfirmerPluginManager(ConfirmerPluginManager $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

}
