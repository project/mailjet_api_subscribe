<?php

namespace Drupal\mailjet_api_subscribe;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\mailjet_api\MailjetApiHandler;
use Mailjet\Client;
use Mailjet\Resources;
use Psr\Log\LoggerInterface;

/**
 * Mail handler to send out an email message array to the Mailjet API.
 *
 * https://dev.mailjet.com/email/guides
 * https://dev.mailjet.com/email/reference
 *
 */
class MailjetApiSubscribeHandler {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $mailjetApiConfig;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Mailjet client.
   *
   * @var \Mailjet\Client
   */
  protected $client;


  /**
   * Constructs a new \Drupal\mailjet_api\MailjetApiHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerInterface $logger) {
    $this->configFactory = $configFactory;
    $this->mailjetApiConfig = $this->configFactory->get('mailjet_api.settings');
    $this->logger = $logger;
    $this->client = new Client(
      $this->mailjetApiConfig->get('api_key_public'),
      $this->mailjetApiConfig->get('api_key_secret'),
      TRUE,
      ['version' => 'v3']
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getContactslist() {
    $lists = [];
    try {
      if (MailjetApiHandler::status() === FALSE) {
        $this->logger->error('Failed to get Lists. Please check the Mailjet API settings.');
        return $lists;
      }

      $response = $this->client->get(Resources::$Contactslist);

      // Debug mode: log all messages.
      if ($this->mailjetApiConfig->get('debug_mode')) {
        $data = print_r($response->getData(), TRUE);
        $this->logger->notice('Requesting Lists. Status code: @status. Data: <pre>@data</pre>',
          [
            '@status' => $response->getStatus(),
            '@data'   => $data,
          ]
        );
      }

      if ($response->getStatus() == '200' || $response->getStatus() == '201') {
        // TODO debug to confirm functionality
        rand();
        $data = $response->getData();

        foreach ($data as $d) {
          $lists[$d['ID']] = $d['Name'];
        }
      }

    } catch (\Exception $e) {
      $this->logger->error('Exception while trying to get Lists. @code: @message.',
        [
          '@code'    => $e->getCode(),
          '@message' => $e->getMessage(),
        ]
      );
    }

    return $lists;
  }


  /**
   * {@inheritdoc}
   */
  public function getTemplates() {
    $templates = [];
    try {
      if (MailjetApiHandler::status() === FALSE) {
        $this->logger->error('Failed to get Templates. Please check the Mailjet API settings.');
        return $templates;
      }

      $response = $this->client->get(Resources::$Template);

      // Debug mode: log all messages.
      if ($this->mailjetApiConfig->get('debug_mode')) {
        $data = print_r($response->getData(), TRUE);
        $this->logger->notice('Requesting Templates. Status code: @status. Data: <pre>@data</pre>',
          [
            '@status' => $response->getStatus(),
            '@data'   => $data,
          ]
        );
      }

      if ($response->getStatus() == '200' || $response->getStatus() == '201') {
        // TODO debug to confirm functionality
        rand();
        $data = $response->getData();

        foreach ($data as $d) {
          $templates[$d['ID']] = $d['Name'];
        }
      }

    } catch (\Exception $e) {
      $this->logger->error('Exception while trying to get Templates. @code: @message.',
        [
          '@code'    => $e->getCode(),
          '@message' => $e->getMessage(),
        ]
      );
    }

    return $templates;
  }

  /**
   * https://dev.mailjet.com/email/reference/contacts/subscriptions#v3_post_contactslist_list_ID_managecontact
   * Will try to subscribe an email to a list Contact will be created on
   * mailjets side or reused if exists
   *
   * {@inheritdoc}
   */
  public function subscribeToList($email, $list_id) {
    $success = NULL;
    try {
      if (MailjetApiHandler::status() === FALSE) {
        $this->logger->error('Failed to get Lists. Please check the Mailjet API settings.');
        return $success;
      }

      $body = [
        'Properties' => "object",
        'Action'     => "addforce",
        'Email'      => $email,
      ];
      $response = $this->client->post(Resources::$ContactslistManagecontact, [
        'id'   => $list_id,
        'body' => $body,
      ]);

      // Debug mode: log all messages.
      if ($this->mailjetApiConfig->get('debug_mode')) {
        $data = print_r($response->getData(), TRUE);
        $this->logger->notice('Requesting Lists. Status code: @status. Data: <pre>@data</pre>',
          [
            '@status' => $response->getStatus(),
            '@data'   => $data,
          ]
        );
      }

      if ($response->getStatus() == '200' || $response->getStatus() == '201') {
        // TODO debug to confirm functionality
        rand();
        $success = $response->getData();
      }

    } catch (\Exception $e) {
      $this->logger->error('Exception while trying to get subscription state for contact %email and list %list_id. @code: @message.',
        [
          '@email'   => $email,
          '@list_id' => $list_id,
          '@code'    => $e->getCode(),
          '@message' => $e->getMessage(),
        ]
      );
    }

    return $success;
  }


}
