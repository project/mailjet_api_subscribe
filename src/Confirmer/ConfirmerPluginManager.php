<?php

namespace Drupal\mailjet_api_subscribe\Confirmer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\mailjet_api_subscribe\Annotation\Confirmer;
use Drupal\Core\StringTranslation\StringTranslationTrait;


/**
 * Manages Confirmer plugins.
 *
 * @see \Drupal\mailjet_api_subscribe\Annotation\Confirmer
 * @see \Drupal\mailjet_api_subscribe\Confirmer\ConfirmerInterface
 * @see \Drupal\mailjet_api_subscribe\Confirmer\ConfirmerPluginBase
 * @see plugin_api
 */
class ConfirmerPluginManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/mailjet_api_subscribe/Confirmer', $namespaces, $module_handler, ConfirmerInterface::class, Confirmer::class);
    $this->setCacheBackend($cache_backend, 'mailjet_api_subscribe_confirmer');
  }

}
