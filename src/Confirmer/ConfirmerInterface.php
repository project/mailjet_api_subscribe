<?php

namespace Drupal\mailjet_api_subscribe\Confirmer;


use Drupal\Core\Form\FormStateInterface;

/**
 * Interface UrlProcessorInterface.
 *
 * The url processor takes care of retrieving facet information from the url.
 * It also handles the generation of facet links. This extends the pre query and
 * build processor interfaces, those methods are where the bulk of the work
 * should be done.
 *
 * The facet manager has one url processor.
 *
 * @package Drupal\facets\UrlProcessor
 */
interface ConfirmerInterface {

  /**
   * Used during building the Form inside the Block
   */
  public function buildForm($form, FormStateInterface $formState);

  public function validateForm(&$form, FormStateInterface $formState);

  public function submitForm(&$form, FormStateInterface $formState);

  /**
   * Settings to configure the Confirmer globally, across all placed blocks
   * which use it. Reachable in Settings in the Admin backend
   */
  public function buildSettingsForm($form, FormStateInterface $formState);

  public function validateSettingsForm(&$form, FormStateInterface $formState);

  public function submitSettingsForm(&$form, FormStateInterface $formState);

}
