<?php

namespace Drupal\mailjet_api_subscribe\Plugin\mailjet_api_subscribe\Confirmer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\mailjet_api_subscribe\Confirmer\ConfirmerPluginBase;


/**
 * Simple Confirmer.
 *
 * @Confirmer(
 *   id = "simple_confirmer",
 *   label = @Translation("Simple Confirmer"),
 *   description = @Translation("Adds the user direcly to a list without any
 *   pre or post processing.")
 * )
 */
class SimpleConfirmer extends ConfirmerPluginBase {

  /**
   * Used during building the Form inside the Block
   */
  public function buildForm($form, FormStateInterface $formState) {
    return $form;
  }

  public function validateForm(&$form, FormStateInterface $formState) {
  }

  public function submitForm(&$form, FormStateInterface $formState) {
    $email = $formState->getValue('email');
    $list_id = $formState->getValue('list_id');
    $mailjetApiSubscribeHandler = \Drupal::service('mailjet_api_subscribe.subscribe_handler');
    $mailjetApiSubscribeHandler->subscribeToList($email, $list_id);
    \Drupal::messenger()->addStatus(t('Subscribed to Newsletter successfully'));
  }

  /**
   * Settings to configure the Confirmer globally, across all placed blocks
   * which use it. Reachable in Settings in the Admin backend
   */
  public function buildSettingsForm($form, FormStateInterface $formState) {
  }

  public function validateSettingsForm(&$form, FormStateInterface $formState) {
  }

  public function submitSettingsForm(&$form, FormStateInterface $formState) {
  }


}
