<?php


namespace Drupal\mailjet_api_subscribe\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\mailjet_api_subscribe\Confirmer\ConfirmerPluginManager;
use Drupal\mailjet_api_subscribe\MailjetApiSubscribeHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Provides a 'Subscribe' block.
 *
 * @Block(
 *   id = "mailjet_api_subscribe_block",
 *   admin_label = @Translation("Mailjet Subscribe"),
 *   category = @Translation("Mailjet"),
 *   module = "mailjet_api_subscribe",
 * )
 */
class SubscribeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Entity type manager.
   *
   * @var \Drupal\core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match.
   *
   * @var Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The route match.
   *
   * @var \Drupal\mailjet_api_subscribe\MailjetApiSubscribeHandler
   */
  protected $mailjetApiSubscribeHandler;

  /**
   * @var ConfirmerPluginManager
   */
  protected $confirmerPluginManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Creates a WebformBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(array                      $configuration, $plugin_id, $plugin_definition,
                              RequestStack               $request_stack,
                              EntityTypeManagerInterface $entity_type_manager,
                              FormBuilderInterface       $form_builder,
                              MailjetApiSubscribeHandler $mailjetApiSubscribeHandler,
                              ConfirmerPluginManager     $confirmerPluginManager,
                              RouteMatchInterface        $route_match = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->mailjetApiSubscribeHandler = $mailjetApiSubscribeHandler;
    $this->confirmerPluginManager = $confirmerPluginManager;
    $this->routeMatch = $route_match ?: \Drupal::routeMatch();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('mailjet_api_subscribe.subscribe_handler'),
      $container->get('plugin.manager.mailjet_api_subscribe.confirmer'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list_id'             => '',
      'confirmer_plugin_id' => 'simple_confirmer',
      'submit_label'        => $this->t('Subscribe'),
      //      'description' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();

    $list_options = $this->mailjetApiSubscribeHandler->getContactslist();
    $form['list_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Mailjet Lists'),
      '#default_value' => $this->configuration['list_id'] ?? $default['list_id'],
      '#options'       => $list_options,
      '#description'   => $this->t('Available mailjet lists.'),
    ];

    $confirmer_options = [];
    $active_plugins = $this->confirmerPluginManager->getDefinitions();
    foreach ($active_plugins as $plugin) {
      $confirmer_options[$plugin['id']] = $plugin['label'];
    }
    $form['confirmer_plugin_id'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Confirmer Plugins'),
      '#default_value' => $this->configuration['confirmer_plugin_id'] ?? $default['confirmer_plugin_id'],
      '#options'       => $confirmer_options,
      '#description'   => $this->t('How is an E-Mail validated.'),
      '#required'      => TRUE,
    ];

    $form['submit_label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Submit Label'),
      '#default_value' => $this->configuration['submit_label'] ?? $default['submit_label'],
      '#description'   => $this->t('Label for the submission button.'),
      '#size'          => 60,
      '#maxlength'     => 128,
      '#required'      => TRUE,
    ];

    /*    $form['description'] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('Description'),
          '#description'   => $this->t('Description text above the form.'),
        ];*/

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['list_id'] = $form_state->getValue('list_id');
    $this->configuration['confirmer_plugin_id'] = $form_state->getValue('confirmer_plugin_id');
    $this->configuration['submit_label'] = $form_state->getValue('submit_label');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $subscribe_form = new \Drupal\mailjet_api_subscribe\Form\SubscribeForm();

    $subscribe_form->setMailjetApiSubscribe($this->mailjetApiSubscribeHandler);
    $subscribe_form->setConfirmerPluginManager($this->confirmerPluginManager);

    $subscribe_form->setListID($this->configuration['list_id']);
    $subscribe_form->setConfirmer($this->configuration['confirmer_plugin_id']);
    $subscribe_form->setSubmitLabel($this->configuration['submit_label']);

    $form = $this->formBuilder->getForm($subscribe_form);

    return $form;
  }

}

