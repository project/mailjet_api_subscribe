<?php

namespace Drupal\mailjet_api_subscribe_double_optin\Plugin\mailjet_api_subscribe\Confirmer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\mailjet_api_subscribe\Confirmer\ConfirmerPluginBase;


/**
 * Double OptIn Confirmer.
 *
 * @Confirmer(
 *   id = "double_optin_confirmer",
 *   label = @Translation("Double OptIn Confirmer"),
 *   description = @Translation("Adds the user to the list by clicking on a a
 *   link in an email which is sent via the contrib module 'Email Confirmer'")
 * )
 */
class DoubleOptInConfirmer extends ConfirmerPluginBase {

  /**
   * Used during building the Form inside the Block
   */
  public function buildForm($form, FormStateInterface $formState) {
    return $form;
  }

  public function validateForm(&$form, FormStateInterface $formState) {
  }

  public function submitForm(&$form, FormStateInterface $formState) {
    $email = $formState->getValue('email');
    $list_id = $formState->getValue('list_id');

    // Starts the confirm process.
    $email_confirmer = \Drupal::service('email_confirmer');
    $email_confirmer->confirm($email, ['list_id' => $list_id], 'mailjet_api_subscribe');
    \Drupal::messenger()->addStatus(t('Confirmation E-Mail has been sent. Click on the link inside the mail to confirm the subscription.'));
  }

  /**
   * Settings to configure the Confirmer globally, across all placed blocks
   * which use it. Reachable in Settings in the Admin backend
   */
  public function buildSettingsForm($form, FormStateInterface $formState) {
  }

  public function validateSettingsForm(&$form, FormStateInterface $formState) {
  }

  public function submitSettingsForm(&$form, FormStateInterface $formState) {
  }


}
